
#include <algorithm>
#include <fmt/core.h>
#include <fmt/xchar.h>
#include <string>
#include <utility>
#include <vector>

// Plot container of unsigned coordinates
std::wstring xy(const std::vector<std::pair<uint32_t, uint32_t>> &in) {

  setlocale(LC_ALL, "en_US.utf8");

  if (in.empty())
    return {};

  const auto W = uint32_t{80};
  const auto H = uint32_t{40};

  // Create a blank plot
  std::vector<std::wstring> plot(H, std::wstring(W, L' '));

  // Plot coordinates, flip Y to make 0,0 bottom left
  std::ranges::for_each(in, [&](const auto &c) {

    const auto &[x1, y1] = c;
    const auto x = x1;
    const auto y = H - y1 - 1;
    static auto previous_y = y;

    const auto marker = wchar_t{L'\u2b24'};

    if (y >= H) {
      fmt::print(L"{},{},{},{},{} out of range\n", x1, y1, x, y, previous_y);
    } else {
      plot[y][x] = marker;
      previous_y = y;
    }
  });

  // Create output string
  std::wstring out;
  for (const auto &row : plot)
    out += L"|" + row + L"|\n";

  // Add borders
  const auto horizonal_border = std::wstring(W + 2, L'_') + L"\n";

  return horizonal_border + out + horizonal_border;
}

#ifndef NDEBUG
#include <cassert>
int main() {

  const auto in = std::vector<std::pair<uint32_t, uint32_t>>{
      {0, 0},   {1, 0},   {2, 1},   {3, 1},   {4, 2},   {5, 2},   {6, 3},
      {7, 3},   {8, 4},   {9, 4},   {10, 5},  {11, 5},  {12, 6},  {13, 6},
      {14, 7},  {15, 7},  {16, 8},  {17, 8},  {18, 9},  {19, 9},  {20, 10},
      {21, 10}, {22, 11}, {23, 11}, {24, 12}, {25, 12}, {26, 13}, {27, 13},
      {28, 14}, {29, 14}, {30, 15}, {31, 15}, {32, 16}, {33, 16}, {34, 17},
      {35, 17}, {36, 18}, {37, 18}, {38, 19}, {39, 19}, {40, 20}, {41, 20},
      {42, 21}, {43, 21}, {44, 22}, {45, 22}, {46, 23}, {47, 23}, {48, 24},
      {49, 24}, {50, 25}, {51, 25}, {52, 26}, {53, 26}, {54, 27}, {55, 27},
      {56, 28}, {57, 28}, {58, 29}, {59, 29}, {60, 30}, {61, 30}, {62, 31},
      {63, 31}, {64, 32}, {65, 32}, {66, 33}, {67, 33}, {68, 34}, {69, 34},
      {70, 35}, {71, 35}, {72, 36}, {73, 36}, {74, 37}, {75, 37}, {76, 38},
      {77, 38}, {78, 39}, {79, 39}};

  fmt::print(L"{}", xy(in));

  assert(xy({}).empty());
  assert(not xy(in).empty());
}
#endif
